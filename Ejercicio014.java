/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author Luis Alonso Teos Palacios 14. Un trabajador recibe su salario normal
 * por las primeras 30 horas y se le paga 1 ½ veces su sueldo normal por cada
 * hora después de las primeras 30. Escriba un programa que calcule e imprima el
 * pago al empleado basado en el sueldo normal y en el número de horas de
 * trabajo, donde estos datos son introducidos por el usuario.
 *
 */
public class Ejercicio014 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double salario, horas, nSalario, horasExtras;
       
        

        Scanner in = new Scanner(System.in);


        System.out.println("Ingrese Su Salario: ");
        salario = in.nextDouble();

        System.out.println("Ingrese Cantidad de Horas extras trabajadas: ");
        horas = in.nextDouble();

        if(horas <=30 ){
        System.out.println("Su Salario es de: $"+salario+", no trabajó horas extras");
            
        }else{
            /*obtenemos la cantidad de horas extras trabajadas*/
            horasExtras = ((salario*1.50)*((int)horas - 30))+salario;
           // System.out.println("Su Salario es de: $"+ horasExtras);
            
             System.out.println("Su Salario es de: $"+ horasExtras+" por haber trabajado "+((int)horas - 30)+" horas extra");
            
        }
       
    }

}
