/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.ejercicios.luisteos;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Luis Alonso Teos Palacios 17. Escriba un programa interactivo para la
 * compañía ABC, que haga procesar y muestre la información de la nómina de sus
 * diez empleados. Para cada empleado, el programa 3 debe leer el nombre, las
 * horas de trabajo, la base de pago (el pago por hora) y la edad. El pago total
 * se calcula con la base del pago por las primeras 40 horas y 1.5 veces el
 * sueldo normal por cada hora encima de las 40. Además, se le retiene un
 * impuesto de la manera siguiente: a. 10% de los primeros $200. b. 20% de la
 * cantidad adicional. La primera parte de la salida de este programa debe
 * mostrar la siguiente información relevante por cada empleado: a. Nombre del
 * empleado. b. Edad del empleado. c. Número de horas trabajadas. d. La base de
 * pago. e. El sueldo total. f. El impuesto retenido. g. El sueldo neto. La
 * segunda parte de este programa debe mostrar: a. El pago total para todos los
 * empleados de al menos 55 años. b. El pago total para los empleados de menos
 * de 55 años. c. El nombre y el pago total para el empleado de al menos 55 años
 * que tenga mayor salario (asegurase que no hay empate). d. El nombre y el pago
 * total para el empleado de menos de 55 años que tenga mayor salario
 * (asegurarse que no hay empate).
 */
public class Ejercicio017 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        ArrayList<empleado> empleados = new ArrayList<empleado>();

        empleado empleado1 = new empleado();
        empleado1.setNombre("Luis");
        empleado1.setHorasTrabajo(55);
        empleado1.setSueldoHora(6.25);
        empleado1.setEdad(19);
        empleados.add(empleado1);

        empleado empleado2 = new empleado();
        empleado2.setNombre("Jose");
        empleado2.setHorasTrabajo(60);
        empleado2.setSueldoHora(5);
        empleado2.setEdad(22);
        empleados.add(empleado2);

        empleado empleado3 = new empleado();
        empleado3.setNombre("Lorena");
        empleado3.setHorasTrabajo(31);
        empleado3.setSueldoHora(9.10);
        empleado3.setEdad(23);
        empleados.add(empleado3);

        empleado empleado4 = new empleado();
        empleado4.setNombre("William");
        empleado4.setHorasTrabajo(90);
        empleado4.setSueldoHora(9);
        empleado4.setEdad(56);
        empleados.add(empleado4);

        empleado empleado5 = new empleado();
        empleado5.setNombre("Rene");
        empleado5.setHorasTrabajo(60);
        empleado5.setSueldoHora(9.99);
        empleado5.setEdad(33);
        empleados.add(empleado5);

        empleado empleado6 = new empleado();
        empleado6.setNombre("Yenni");
        empleado6.setHorasTrabajo(65);
        empleado6.setSueldoHora(70);
        empleado6.setEdad(21);
        empleados.add(empleado6);

        empleado empleado7 = new empleado();
        empleado7.setNombre("Nohemi");
        empleado7.setHorasTrabajo(80);
        empleado7.setSueldoHora(2.10);
        empleado7.setEdad(30);
        empleados.add(empleado7);

        empleado empleado8 = new empleado();
        empleado8.setNombre("Alberto");
        empleado8.setHorasTrabajo(50);
        empleado8.setSueldoHora(6.15);
        empleado8.setEdad(60);
        empleados.add(empleado8);

        empleado empleado9 = new empleado();
        empleado9.setNombre("Angel");
        empleado9.setHorasTrabajo(100);
        empleado9.setSueldoHora(1.12);
        empleado9.setEdad(25);
        empleados.add(empleado9);

        empleado empleado10 = new empleado();
        empleado10.setNombre("Nancy");
        empleado10.setHorasTrabajo(20);
        empleado10.setSueldoHora(5.50);
        empleado10.setEdad(23);
        empleados.add(empleado10);

        System.out.println("Lista de Empleados");
        System.out.println("-----------------------------");

        /*El pago total se calcula con la base del pago por las primeras 40 horas y 1.5 veces el
          sueldo normal por cada hora encima de las 40. Además, se le retiene un impuesto de
          la manera siguiente:
          a. 10% de los primeros $200.
          b. 20% de la cantidad adicional. */
          /*totales salarios*/
            double totalMayor55=0;
            double totalMenor55=0;
            
            double salarioMayor;
            double salarioMenor;
            int idEmpleado;
            int cnt;
            getDatos empleadoMinMax = new getDatos();

        for (empleado thisEmpleado : empleados) {
            double salarioBase = 0;
            double salarioExtra = 0;
            int horaAdicional = 0;
            double salarioTotal = 0;
            double impuestoTotalRetenido = 0;
            double impuestoP200 = 0;
            double impuestoResto = 0;
            double salarioNeto = 0;
            cnt=0;
            
            
          

            System.out.println("Empleado: " + thisEmpleado.getNombre());
            System.out.println("Edad: " + thisEmpleado.getEdad());
            System.out.println("Horas Trabajadas: " + thisEmpleado.getHorasTrabajo());
            System.out.println("Sueldo por Hora: " + thisEmpleado.getSueldoHora());

            if (thisEmpleado.getHorasTrabajo() > 40) {
                salarioBase = thisEmpleado.getSueldoHora() * thisEmpleado.getHorasTrabajo();
                horaAdicional = (int) thisEmpleado.getHorasTrabajo() - 40;
                salarioExtra = (salarioBase * 1.50) * horaAdicional;
                salarioTotal = salarioBase + salarioExtra;
                System.out.println("Sueldo Total: " + salarioTotal);

            } else {
                salarioBase = thisEmpleado.getSueldoHora() * thisEmpleado.getHorasTrabajo();
                salarioTotal = salarioBase;
                System.out.println("Sueldo Total: " + salarioBase);

            }
            if (salarioTotal >= 200) {
                impuestoP200 = 200 * 0.10;
                impuestoResto = (salarioTotal - 200) * 0.20;
                impuestoTotalRetenido = impuestoP200 + impuestoResto;
                salarioNeto = salarioTotal - impuestoTotalRetenido;

                System.out.println("Impuesto Retenido: " + impuestoTotalRetenido);
                System.out.println("Sueldo Neto: " + salarioNeto);

            } else {
                System.out.println("Impuesto Retenido: " + 0);
                System.out.println("Sueldo Neto: " + salarioTotal);
            }
            
            if(thisEmpleado.getEdad()>55){
                totalMayor55=totalMayor55+salarioTotal;
            }else{
                totalMenor55=totalMenor55+salarioTotal;
            }
            
            /*mandamos a la funcion que determine el empleado con mayor o menor suelido en la tabla*/
            empleadoMinMax.setNumMax(salarioTotal,thisEmpleado.getNombre());
            empleadoMinMax.setNumMin(salarioTotal,thisEmpleado.getNombre());
         
          
           cnt++;
            
            

            // System.out.println("Sueldo Total: "+thisEmpleado.getSueldoHora());
            System.out.println("-------------------");

        }
        System.out.println("Totales Generales de los Empleados");
        System.out.println("-----------------------------");
        System.out.println("Total Salarios de empleados mayores de 55 años: "+totalMayor55);
        System.out.println("Total Salarios de empleados menores de 55 años: "+totalMenor55);
      
        
       System.out.println("El Empleado con el MAYOR salario es: "+empleadoMinMax.getEmpleadoMax()+" con "+empleadoMinMax.getNumMax());
       System.out.println("El Empleado con el MENOR salario es: "+empleadoMinMax.getEmpleadoMin()+" con "+empleadoMinMax.getNumMin());
 
   }

    private static class empleado {

        private String nombre;
        private double horasTrabajo;
        private double sueldoHora;
        private int edad;


        /*geters y seters*/
        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getNombre() {
            return this.nombre;
        }

        public void setHorasTrabajo(double horasTrabajo) {
            this.horasTrabajo = horasTrabajo;
        }

        public double getHorasTrabajo() {
            return this.horasTrabajo;
        }

        public void setSueldoHora(double sueldoHora) {
            this.sueldoHora = sueldoHora;
        }

        public double getSueldoHora() {
            return this.sueldoHora;
        }

        public void setEdad(int edad) {
            this.edad = edad;
        }

        public int getEdad() {
            return this.edad;
        }

    ;

}
     private static class getDatos {

        private double numMax;
        private double numMin;
        private String empleadoMax;
        private String empleadoMin;
        
        getDatos(){
            numMax = 0;
            numMin = 999999999; /* efe */
        }

        public void setNumMax(double numMax, String empleado) {
            if(numMax > this.numMax){
              this.numMax = numMax;
              this.empleadoMax = empleado;
            }
           
        }

        public double getNumMax() {
            return this.numMax;
        }
        
       public void setNumMin(double numMin, String empleado) {
           
           if (numMin < this.numMin){
                this.numMin = numMin;
                this.empleadoMin=empleado;
           }

           
        }

        public double getNumMin() {
            return this.numMin;
        }
        
       public String getEmpleadoMax() {
            return this.empleadoMax;
        }
        
   

        public String getEmpleadoMin() {
            return this.empleadoMin;
        }
        
        
    }

}
