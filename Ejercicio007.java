/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author inf003
 */
public class Ejercicio007 {

    /**
     * @author Luis Alonso Teos Palacios 7. Realice un programa que solicite el
     * nombre de una persona, su edad, sexo y teléfono, si el sexo es masculino,
     * que muestre en pantalla el mensaje de “El Sr. (nombre) ha sido
     * registrado” y si el sexo es femenino que muestre el mensaje “La Sra.
     * (nombre) ha sido registrada”.      *
     */
    public static void main(String[] args) {
        String nombres;
        String sexo;
        String telefono;
        int edad;

        Scanner in = new Scanner(System.in);

        System.out.println("Digite su nombre: ");
        nombres = in.nextLine();
        System.out.println("Digite su  sexo (Masculino/Femenino): ");
        sexo = in.nextLine();
        System.out.println("Digite Su telefono (+503XXXXXXXX)");
        telefono = in.nextLine();
        System.out.println("Digite su edad: ");
        edad = in.nextInt();

        if (sexo.equalsIgnoreCase("masculino")) {
            System.out.println("El Sr. " + nombres + " ha sido registrado");

        } else if (sexo.equalsIgnoreCase("femenino")) {
            System.out.println("La Sra. " + nombres + " ha sido registrada");

        } else {
            System.out.println("La persona " + nombres + " ha sido registrado/a");

        }

    }

}
