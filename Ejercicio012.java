/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author Luis Alonso Teos Palacios 12. En una universidad, los alumnos de
 * antiguo ingreso solo pagan $30 por asignatura, mientras que los de nuevo
 * ingreso pagan $50 por asignatura. Escriba un programa en el cual el usuario
 * introduce la categoría del estudiante (A por antiguo ingreso y N por nuevo
 * ingreso), y el número de asignaturas. La salida debe mostrar la categoría del
 * estudiante, el número de asignaturas y el total a pagar.
 *
 */
public class Ejercicio012 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String nombre, categoria;
        int asignaturas;

        Scanner in = new Scanner(System.in);

        System.out.println("Ingrese Nombre del estudiante: ");
        nombre = in.nextLine();

        System.out.println("Ingrese Categoria (A = Antiguo ingreso, N = Nuevo Ingreso): ");
        categoria = in.nextLine();

        System.out.println("Ingrese Cantidad de Asignaturas: ");
        asignaturas = in.nextInt();

        if (categoria.equalsIgnoreCase("A")) {
            System.out.println("El Total a pagar por " + asignaturas + " paras ANTIGUO INGRESO es: $" + asignaturas * 30);

        } else if (categoria.equalsIgnoreCase("N")) {
            System.out.println("El Total a pagar por " + asignaturas + " paras NUEVO INGRESO es: $" + asignaturas * 50);

        } else {
            System.out.println("Categoria Invalida...");

        }
    }

}
