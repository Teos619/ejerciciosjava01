/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Luis Alonso Teos Palacios 18. Escriba un programa interactivo para
 * calificar a un grupo de 10 alumnos. El programa debe leer el nombre las tres
 * calificaciones para cada alumno. Debe calcular el promedio para cada uno y
 * después determinar si el alumno es aprobado o reprobado. Se requiere un
 * promedio de al menos 60 para aprobar. La primera parte de la salida debe ser:
 * a. El nombre del alumno. b. Las tres calificaciones del alumno. c. El
 * promedio obtenido. d. El mensaje (aprobado, reprobado) de acuerdo a su
 * promedio. La segunda parte de la salida debe ser: a. El número de alumnos
 * aprobados. b. El número de alumnos reprobados. c. El número de alumnos que
 * obtuvieron un promedio de al menos 80.
 */
public class Ejercicio018 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        ArrayList<alumno> alumnos = new ArrayList<alumno>();

        alumno alumno1 = new alumno();
        alumno1.setNombre("Luis");
        alumno1.setNota1(50.10);
        alumno1.setNota2(60);
        alumno1.setNota3(75);
        alumnos.add(alumno1);

        alumno alumno2 = new alumno();
        alumno2.setNombre("Jose");
        alumno2.setNota1(80);
        alumno2.setNota2(78);
        alumno2.setNota3(33);
        alumnos.add(alumno2);

        alumno alumno3 = new alumno();
        alumno3.setNombre("Lorena");
        alumno3.setNota1(60);
        alumno3.setNota2(80);
        alumno3.setNota3(100);
        alumnos.add(alumno3);

        alumno alumno4 = new alumno();
        alumno4.setNombre("William");
        alumno4.setNota1(90);
        alumno4.setNota2(85);
        alumno4.setNota3(10);
        alumnos.add(alumno4);

        alumno alumno5 = new alumno();
        alumno5.setNombre("Rene");
        alumno5.setNota1(30);
        alumno5.setNota2(40);
        alumno5.setNota3(20);
        alumnos.add(alumno5);

        alumno alumno6 = new alumno();
        alumno6.setNombre("Yenni");
        alumno6.setNota1(70);
        alumno6.setNota2(50);
        alumno6.setNota3(95);
        alumnos.add(alumno6);

        alumno alumno7 = new alumno();
        alumno7.setNombre("Nohemi");
        alumno7.setNota1(50);
        alumno7.setNota2(80);
        alumno7.setNota3(100);
        alumnos.add(alumno7);

        alumno alumno8 = new alumno();
        alumno8.setNombre("Alberto");
        alumno8.setNota1(50);
        alumno8.setNota2(90);
        alumno8.setNota3(70);
        alumnos.add(alumno8);

        alumno alumno9 = new alumno();
        alumno9.setNombre("Angel");
        alumno9.setNota1(99);
        alumno9.setNota2(98);
        alumno9.setNota3(100);
        alumnos.add(alumno9);

        alumno alumno10 = new alumno();
        alumno10.setNombre("Nancy");
        alumno10.setNota1(50);
        alumno10.setNota2(76);
        alumno10.setNota3(97);
        alumnos.add(alumno10);

        System.out.println("Lista de Alumnos");
        System.out.println("-----------------------------");
        
        int cntAprobados =0;
        int cntReprobados =0;
        int cntPromedio80 =0;

        for (alumno thisAlumno : alumnos) {

            System.out.println("Alumno: " + thisAlumno.getNombre());
            System.out.println("Nota1: " + thisAlumno.getNota1());
            System.out.println("Nota2: " + thisAlumno.getNota2());
            System.out.println("Nota3: " + thisAlumno.getNota3());
            System.out.println("Promedio: " + thisAlumno.getPromedio());
            System.out.println("Estado: " + thisAlumno.getMensaje());
            System.out.println("-----------------------------");
            
           if(thisAlumno.getEstado()){
               cntAprobados++;
           }else{
              cntReprobados++;
           }
           
           if(thisAlumno.getPromedio()>=80){
           cntPromedio80++;
           }
            
            

        }
        System.out.println("-----------------------------");
        System.out.println("Estadisticas");
        System.out.println("-----------------------------");
        System.out.println("Total Alumnos Aprobados: " + cntAprobados);
        System.out.println("Total Alumnos Reprobados: " + cntReprobados);
        System.out.println("Total Alumnos Promedio mayor a 80: " + cntPromedio80);

    }

    private static class alumno {

        private String nombre;
        private double nota1;
        private double nota2;
        private double nota3;
        private double promedio;

        /*geters y seters*/
        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getNombre() {
            return this.nombre;
        }

        public void setNota1(double nota) {
            this.nota1 = nota;
        }

        public void setNota2(double nota) {
            this.nota2 = nota;
        }

        public void setNota3(double nota) {
            this.nota3 = nota;
        }

        public Double getNota1() {
            return this.nota1;
        }

        public Double getNota2() {
            return this.nota2;
        }

        public Double getNota3() {
            return this.nota3;
        }

        public double getPromedio() {

            this.promedio = (this.nota1 + this.nota2 + this.nota3) / 3;
            return this.promedio;

        }

        public boolean getEstado() {

            if (this.promedio >= 60) {
                return true;
            } else {
                return false;
            }

        }

        public String getMensaje() {

            if (this.promedio >= 60) {
                return "Aprobado";
            } else {
                return "Reprobado";
            }

        }

    ;

}

}
