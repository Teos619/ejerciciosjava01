/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author Luis Alonso Teos Palacios 10. Escriba un programa que reciba como
 * entrada dos enteros y determine si son del mismo signo o de signos
 * diferentes. entiendo que determine si 2 numeros son iguales, por que los
 * numeros enteros son todos positivos
 *
 */
public class Ejercicio010 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double num1, num2;

        Scanner in = new Scanner(System.in);

        System.out.println("Digite primer numero ");
        num1 = in.nextDouble();

        System.out.println("Digite segundo numero ");
        num2 = in.nextDouble();

        if (num1 == num2) {
            System.out.println("Los numeros son iguales ");
        } else {
            System.out.println("Los numeros NO son iguales ");
        }

    }

}
