/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author Luis Alonso Teos Palacios 2. Escriba un programa que pida un nombre y
 * luego envíe un saludo utilizando ese nombre.
 */
public class Ejercicio002 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String nombre;

        Scanner in = new Scanner(System.in);

        System.out.println("Digite un nombre");
        nombre = in.nextLine();
        System.out.println(nombre);

    }

}
