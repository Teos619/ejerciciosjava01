/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.ejercicios.luisteos;

import java.util.ArrayList;

/**
 *
 * @author Luis Alonso Teos Palacios 15. Escriba un programa que produzca la
 * tabla siguiente: Escriba un programa de nómina para una compañía que tiene
 * seis empleados, algunos de los cuales son hombres y otros son mujeres. Para
 * cada empleado, el programa debe leer el nombre, el sexo, las horas de trabajo
 * y el sueldo por hora. En la primera parte de la salida de este programa se
 * debe mostrar el nombre del empleado, el número de horas trabajadas, el pago
 * por hora y el pago total. En la segunda parte de la salida de este programa
 * se debe mostrar el total y el promedio de pagos por sexo.
 */
public class Ejercicio016 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        ArrayList<empleado> empleados = new ArrayList<empleado>();

        empleado empleado1 = new empleado();
        empleado1.setNombre("Luis");
        empleado1.setSexo("M");
        empleado1.setSueldo(150);
        empleado1.setHorasTrabajo(3);
        empleados.add(empleado1);

        empleado empleado2 = new empleado();
        empleado2.setNombre("Jose");
        empleado2.setSexo("M");
        empleado2.setSueldo(250);
        empleado2.setHorasTrabajo(5);
        empleados.add(empleado2);

        empleado empleado3 = new empleado();
        empleado3.setNombre("Lorena");
        empleado3.setSexo("F");
        empleado3.setSueldo(210);
        empleado3.setHorasTrabajo(4);
        empleados.add(empleado3);

        empleado empleado4 = new empleado();
        empleado4.setNombre("William");
        empleado4.setSexo("M");
        empleado4.setSueldo(300);
        empleado4.setHorasTrabajo(2);
        empleados.add(empleado4);

        empleado empleado5 = new empleado();
        empleado5.setNombre("Rene");
        empleado5.setSexo("M");
        empleado5.setSueldo(130);
        empleado5.setHorasTrabajo(5);
        empleados.add(empleado5);

        empleado empleado6 = new empleado();
        empleado6.setNombre("Yenni");
        empleado6.setSexo("F");
        empleado6.setSueldo(400);
        empleado6.setHorasTrabajo(4);
        empleados.add(empleado6);

        System.out.println("Lista de Empleados");
        System.out.println("-----------------------------");

        for (empleado thisEmpleado : empleados) {
            System.out.println("Empleado: " + thisEmpleado.getNombre());
            System.out.println("Horas Trabajadas: " + thisEmpleado.getHorasTrabajo());
            System.out.println("Sueldo por hora: " + thisEmpleado.getSueldo());
            System.out.println("-------------------");

        }
        System.out.println("Total y Promedio de Pagos Por Sexo");
        System.out.println("-----------------------------");

        double totalM, totalF;
        int cntM, cntF;
        totalM = 0;
        totalF = 0;
        cntM = 0;
        cntF = 0;
        /*obtenemos los datos a calcular*/
        for (empleado thisEmpleado : empleados) {

            if (thisEmpleado.getSexo().equalsIgnoreCase("M")) {
                totalM = totalM + thisEmpleado.getSueldo();
                cntM = cntM + 1;
            } else {
                totalF = totalF + thisEmpleado.getSueldo();
                cntF = cntF + 1;
            }

        }
        System.out.println("Total de Pagos en el Sexo Masculino(" + cntM + "): $" + totalM + " con un promedio de: $" + (totalM / cntM));
        System.out.println("Total de Pagos en el Sexo Femenino(" + cntF + "): $" + totalF + " con un promedio de: $" + (totalF / cntF));

    }

    private static class empleado {

        private String nombre;
        private String sexo;
        private double sueldo;
        private double horasTrabajo;

        /*geters y seters*/
        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getNombre() {
            return this.nombre;
        }

        public void setSexo(String sexo) {
            this.sexo = sexo;
        }

        public String getSexo() {
            return this.sexo;
        }

        public void setSueldo(double sueldo) {
            this.sueldo = sueldo;
        }

        public double getSueldo() {
            return this.sueldo;
        }

        public void setHorasTrabajo(double horasTrabajo) {
            this.horasTrabajo = horasTrabajo;
        }

        public double getHorasTrabajo() {
            return this.horasTrabajo;
        }

    ;

}

}
