/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author Luis Alonso Teos Palacios Escriba un programa que reciba como entrada
 * dos enteros positivos distintos y escriba la diferencia entre el número mayor
 * y el número menor. Nota: la diferencia debe de ser siempre un valor positivo.
 *
 */
public class Ejercicio013 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int num1, num2;

        Scanner in = new Scanner(System.in);

        System.out.println("Ingrese Primer Numero: ");
        num1 = in.nextInt();
        System.out.println("Ingrese Segundo Numero: ");
        num2 = in.nextInt();

        if (num1 == num2) {
            System.out.println("No hay diferencia entre los numeros");
        } else if (num1 > num2) {
            System.out.println("La diferencia entre los numeros es: " + (num1 - num2) + " El Primer Numero es Mayor");

        } else {
            System.out.println("La diferencia entre los numeros es: " + (num2 - num1) + " El Segundo Numero es Mayor");
        }

    }

}
