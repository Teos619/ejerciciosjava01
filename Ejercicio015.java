/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;



/**
 *
 * @author Luis Alonso Teos Palacios 15. Escriba un programa que produzca la tabla siguiente:
Yardas Pulgadas 
* 1 36 
* 2 72 
* 3 108 
* . . 
* 10 360
 *
 */
public class Ejercicio015 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("Yardas -> Pulgadas");
        for (int i = 1; i <= 10; i++) {

            System.out.println(i + "->" + (i * 36));

        }

    }

}
