/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.ejercicios.luisteos;

import java.util.ArrayList;

/**
 *
 * @author Luis Alonso Teos Palacios 18. Escriba un programa interactivo para
 * calificar a un grupo de 10 alumnos. El programa debe leer el nombre las tres
 * calificaciones para cada alumno. Debe calcular el promedio para cada uno y
 * después determinar si el alumno es aprobado o reprobado. Se requiere un
 * promedio de al menos 60 para aprobar. La primera parte de la salida debe ser:
 * a. El nombre del alumno. b. Las tres calificaciones del alumno. c. El
 * promedio obtenido. d. El mensaje (aprobado, reprobado) de acuerdo a su
 * promedio. La segunda parte de la salida debe ser: a. El número de alumnos
 * aprobados. b. El número de alumnos reprobados. c. El número de alumnos que
 * obtuvieron un promedio de al menos 80.
 */
public class Ejercicio018_v2 {
    
  static ArrayList<alumno> listaAlumnos = new ArrayList<alumno>();

    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        nuevoAlumno("Luis",50.10,60,75);
        nuevoAlumno("Jose",80,78,33);
        nuevoAlumno("Lorena",60,80,100);
        nuevoAlumno("William",90,85,10);
        nuevoAlumno("Rene",30,40,20);
        nuevoAlumno("Yenni",70,50,95);
        nuevoAlumno("Nohemi",50,80,100);
        nuevoAlumno("Alberto",50,90,70);
        nuevoAlumno("Angel",98,99,100);
        nuevoAlumno("Nancy",50,76,97);



        System.out.println("Lista de Alumnos");
        System.out.println("-----------------------------");
        calculos();

    }
        /*funcion que solo sirve para llenar el arraylist*/
    private static void nuevoAlumno(String nombre, double nota1, double nota2, double nota3){

        alumno thisAlumno = new alumno();
        thisAlumno.setNombre(nombre);
        thisAlumno.setNota1(nota1);
        thisAlumno.setNota2(nota2);
        thisAlumno.setNota3(nota3);
        listaAlumnos.add(thisAlumno);

    }
    private static void calculos() {
       
        int cntAprobados =0;
        int cntReprobados =0;
        int cntPromedio80 =0;

        for (alumno thisAlumno : listaAlumnos) {

            System.out.println("Alumno: " + thisAlumno.getNombre());
            System.out.println("Nota1: " + thisAlumno.getNota1());
            System.out.println("Nota2: " + thisAlumno.getNota2());
            System.out.println("Nota3: " + thisAlumno.getNota3());
            System.out.println("Promedio: " + thisAlumno.getPromedio());
            System.out.println("Estado: " + thisAlumno.getMensaje());
            System.out.println("-----------------------------");
            
           if(thisAlumno.getEstado()){
               cntAprobados++;
           }else{
              cntReprobados++;
           }
           
           if(thisAlumno.getPromedio()>=80){
           cntPromedio80++;
           }
            
            

        }
        System.out.println("-----------------------------");
        System.out.println("Estadisticas");
        System.out.println("-----------------------------");
        System.out.println("Total Alumnos Aprobados: " + cntAprobados);
        System.out.println("Total Alumnos Reprobados: " + cntReprobados);
        System.out.println("Total Alumnos Promedio mayor a 80: " + cntPromedio80);
    
    }

    private static class alumno {

        private String nombre;
        private double nota1;
        private double nota2;
        private double nota3;
        private double promedio;

        /*geters y seters*/
        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getNombre() {
            return this.nombre;
        }

        public void setNota1(double nota) {
            this.nota1 = nota;
        }

        public void setNota2(double nota) {
            this.nota2 = nota;
        }

        public void setNota3(double nota) {
            this.nota3 = nota;
        }

        public Double getNota1() {
            return this.nota1;
        }

        public Double getNota2() {
            return this.nota2;
        }

        public Double getNota3() {
            return this.nota3;
        }

        public double getPromedio() {

            this.promedio = (this.nota1 + this.nota2 + this.nota3) / 3;
            return this.promedio;

        }

        public boolean getEstado() {

            if (this.promedio >= 60) {
                return true;
            } else {
                return false;
            }

        }

        public String getMensaje() {

            if (this.promedio >= 60) {
                return "Aprobado";
            } else {
                return "Reprobado";
            }

        }

    ;

}

}
