/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author Luis Alonso Teos Palacios 4. Elaborar un programa que permita
 * convertir una medida escogiendo las unidades actuales y el tipo de conversión
 * que desea. Conversiones de centímetro a pulgadas, metros a pies, pies a
 * pulgadas, metros a centímetros.
 */
public class Ejercicio004 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*primero seleccionamos lo que queremos compartir*/
        int opcion;
        float longitud;
        float resultado;
        Scanner in = new Scanner(System.in);
        System.out.println("Bienvenido! Seleccione una Opcion");
        System.out.println("1 - Conversión de Centimetros a Pulgadas");
        System.out.println("2 - Conversión de Metros a Pies");
        System.out.println("3 - Conversión de Pies A Pulgadas");
        System.out.println("4 - Conversión de Metros a Centimetros");
        System.out.println("----------------------------------");

        System.out.println("Digite 1-4");
        opcion = in.nextInt();

        if (opcion == 1) {
            /*Centimetros a Pulgadas*/
            System.out.println("Digite Longitud en Centimetros");
            longitud = in.nextFloat();
            resultado = (float) (longitud / 2.54);
            System.out.println(longitud + " Centimetros son " + resultado + " Pulgadas");

        } else if (opcion == 2) {
            /*Metros a Pies*/
            System.out.println("Digite Longitud en Metros");
            longitud = in.nextFloat();
            resultado = (float) (longitud * 3.281);
            System.out.println(longitud + " Metros Son " + resultado + " Pies");

        } else if (opcion == 3) {
            /*Conversión de Pies A Pulgadas*/
            System.out.println("Digite Longitud en Pies");
            longitud = in.nextFloat();
            resultado = (float) (longitud * 12);
            System.out.println(longitud + " Pies Son " + resultado + " Pulgadas");

        } else if (opcion == 4) {
            /*Metros a Centimetros*/
            System.out.println("Digite Longitud en Metros");
            longitud = in.nextFloat();
            resultado = (float) (longitud * 100);
            System.out.println(longitud + " Metros Son " + resultado + " Centimetros");

        } else {
            System.out.println("Opción no Valida...");
        }

    }

}
