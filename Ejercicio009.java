/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author Luis Alonso Teos Palacios 9. Escriba un programa para conversión de
 * temperaturas. En este caso se puede convertir de grados Fahrenheit a grados
 * Celsius y de grados Celsius a Fahrenheit.
 */
public class Ejercicio009 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int opcion;
        float temperatura;
        float resultado;
        Scanner in = new Scanner(System.in);
        System.out.println("Bienvenido! Seleccione una Opcion");
        System.out.println("1 - Conversión de Fahrenheit a Grados Celsius");
        System.out.println("2 - Conversión de Grados Celsius a Fahrenheit");
        System.out.println("----------------------------------");

        System.out.println("Digite 1-2");
        opcion = in.nextInt();

        if (opcion == 1) {

            System.out.println("Digite Temperatura en Fahrenheit");
            temperatura = in.nextFloat();
            resultado = (float) ((temperatura - 32) * 0.55556);
            System.out.println(temperatura + " Fahrenheit son " + resultado + " Grados Celsius");

        } else if (opcion == 2) {

            System.out.println("Digite Temperatura en Grados Celsius");
            temperatura = in.nextFloat();
            resultado = (float) ((temperatura * 1.8) + 32);
            System.out.println(temperatura + " Grados Celsius Son " + resultado + " Fahrenheit");

        } else {
            System.out.println("Opción no Valida...");
        }

    }

}
