/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author Luis Alonso Teos Palacios 11. Escriba un programa que reciba el peso
 * de una carta en onzas e imprima el costo del porte calculado según la regla
 * siguiente: a. La primera onza cuesta $0.25. b. Cada onza adicional cuesta
 * $0.04.  *
 */
public class Ejercicio011 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double peso, precio;

        Scanner in = new Scanner(System.in);

        System.out.println("Digite el Peso en Onzas ");
        peso = in.nextDouble();

        // System.out.println((int)peso);
        precio = 0;
        for (int i = (int) peso; i > 0; --i) {
            /*la idea es que pase a enteros la onzas, y que al final agrege lo que cuesta una sola onza*/

            if (i == 1) {
                precio = precio + 0.25;
            } else {
                precio = precio + 0.04;
            }

        }
        System.out.println("$" + precio);
    }

}
