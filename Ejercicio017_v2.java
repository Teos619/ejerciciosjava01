/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.ejercicios.luisteos;

import java.util.ArrayList;

/**
 *
 * @author Luis Alonso Teos Palacios 17. Escriba un programa interactivo para la
 * compañía ABC, que haga procesar y muestre la información de la nómina de sus
 * diez empleados. Para cada empleado, el programa 3 debe leer el nombre, las
 * horas de trabajo, la base de pago (el pago por hora) y la edad. El pago total
 * se calcula con la base del pago por las primeras 40 horas y 1.5 veces el
 * sueldo normal por cada hora encima de las 40. Además, se le retiene un
 * impuesto de la manera siguiente: a. 10% de los primeros $200. b. 20% de la
 * cantidad adicional. La primera parte de la salida de este programa debe
 * mostrar la siguiente información relevante por cada empleado: a. Nombre del
 * empleado. b. Edad del empleado. c. Número de horas trabajadas. d. La base de
 * pago. e. El sueldo total. f. El impuesto retenido. g. El sueldo neto. La
 * segunda parte de este programa debe mostrar: a. El pago total para todos los
 * empleados de al menos 55 años. b. El pago total para los empleados de menos
 * de 55 años. c. El nombre y el pago total para el empleado de al menos 55 años
 * que tenga mayor salario (asegurase que no hay empate). d. El nombre y el pago
 * total para el empleado de menos de 55 años que tenga mayor salario
 * (asegurarse que no hay empate).
 */
public class Ejercicio017_v2 {

    static ArrayList<empleado> listaEmpleados = new ArrayList<empleado>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        nuevoEmpleado("Luis", 55, 6.25, 19);
        nuevoEmpleado("Jose", 60, 5, 22);
        nuevoEmpleado("Lorena", 31, 9.10, 23);
        nuevoEmpleado("William", 90, 9, 56);
        nuevoEmpleado("Rene", 60, 9.99, 33);
        nuevoEmpleado("Yenni", 65, 70, 21);
        nuevoEmpleado("Nohemi", 80, 2.10, 30);
        nuevoEmpleado("Alberto", 50, 6.15, 60);
        nuevoEmpleado("Angel", 100, 1.12, 25);
        nuevoEmpleado("Nancy", 20, 5.50, 23);

        System.out.println("Lista de Empleados");
        System.out.println("-----------------------------");
        calculosGenerales();

    }

    /*funcion que solo sirve para llenar el arraylist*/
    private static void nuevoEmpleado(String nombre, double horasTrabajo, double sueldoHora, int edad) {

        empleado thisEmpleado = new empleado();
        thisEmpleado.setNombre(nombre);
        thisEmpleado.setHorasTrabajo(horasTrabajo);
        thisEmpleado.setSueldoHora(sueldoHora);
        thisEmpleado.setEdad(edad);
        listaEmpleados.add(thisEmpleado);

    }

    private static void calculosGenerales() {

        /*El pago total se calcula con la base del pago por las primeras 40 horas y 1.5 veces el
          sueldo normal por cada hora encima de las 40. Además, se le retiene un impuesto de
          la manera siguiente:
          a. 10% de los primeros $200.
          b. 20% de la cantidad adicional. */
 /*totales salarios*/
        double totalMayor55 = 0;
        double totalMenor55 = 0;

        double salarioMayor;
        double salarioMenor;
        int idEmpleado;
        int cnt;
        getDatos empleadoMinMax = new getDatos();

        for (empleado thisEmpleado : listaEmpleados) {
            double salarioBase = 0;
            double salarioExtra = 0;
            int horaAdicional = 0;
            double salarioTotal = 0;
            double impuestoTotalRetenido = 0;
            double impuestoP200 = 0;
            double impuestoResto = 0;
            double salarioNeto = 0;
            cnt = 0;

            System.out.println("Empleado: " + thisEmpleado.getNombre());
            System.out.println("Edad: " + thisEmpleado.getEdad());
            System.out.println("Horas Trabajadas: " + thisEmpleado.getHorasTrabajo());
            System.out.println("Sueldo por Hora: " + thisEmpleado.getSueldoHora());

            if (thisEmpleado.getHorasTrabajo() > 40) {
                salarioBase = thisEmpleado.getSueldoHora() * thisEmpleado.getHorasTrabajo();
                horaAdicional = (int) thisEmpleado.getHorasTrabajo() - 40;
                salarioExtra = (salarioBase * 1.50) * horaAdicional;
                salarioTotal = salarioBase + salarioExtra;
                System.out.println("Sueldo Total: " + salarioTotal);

            } else {
                salarioBase = thisEmpleado.getSueldoHora() * thisEmpleado.getHorasTrabajo();
                salarioTotal = salarioBase;
                System.out.println("Sueldo Total: " + salarioBase);

            }
            if (salarioTotal >= 200) {
                impuestoP200 = 200 * 0.10;
                impuestoResto = (salarioTotal - 200) * 0.20;
                impuestoTotalRetenido = impuestoP200 + impuestoResto;
                salarioNeto = salarioTotal - impuestoTotalRetenido;

                System.out.println("Impuesto Retenido: " + impuestoTotalRetenido);
                System.out.println("Sueldo Neto: " + salarioNeto);

            } else {
                System.out.println("Impuesto Retenido: " + 0);
                System.out.println("Sueldo Neto: " + salarioTotal);
            }

            if (thisEmpleado.getEdad() > 55) {
                totalMayor55 = totalMayor55 + salarioTotal;
            } else {
                totalMenor55 = totalMenor55 + salarioTotal;
            }

            /*mandamos a la funcion que determine el empleado con mayor o menor suelido en la tabla*/
            empleadoMinMax.setNumMax(salarioTotal, thisEmpleado.getNombre());
            empleadoMinMax.setNumMin(salarioTotal, thisEmpleado.getNombre());

            cnt++;

            // System.out.println("Sueldo Total: "+thisEmpleado.getSueldoHora());
            System.out.println("-------------------");

        }
        System.out.println("Totales Generales de los Empleados");
        System.out.println("-----------------------------");
        System.out.println("Total Salarios de empleados mayores de 55 años: " + totalMayor55);
        System.out.println("Total Salarios de empleados menores de 55 años: " + totalMenor55);

        System.out.println("El Empleado con el MAYOR salario es: " + empleadoMinMax.getEmpleadoMax() + " con " + empleadoMinMax.getNumMax());
        System.out.println("El Empleado con el MENOR salario es: " + empleadoMinMax.getEmpleadoMin() + " con " + empleadoMinMax.getNumMin());

    }

    private static class empleado {

        private String nombre;
        private double horasTrabajo;
        private double sueldoHora;
        private int edad;


        /*geters y seters*/
        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getNombre() {
            return this.nombre;
        }

        public void setHorasTrabajo(double horasTrabajo) {
            this.horasTrabajo = horasTrabajo;
        }

        public double getHorasTrabajo() {
            return this.horasTrabajo;
        }

        public void setSueldoHora(double sueldoHora) {
            this.sueldoHora = sueldoHora;
        }

        public double getSueldoHora() {
            return this.sueldoHora;
        }

        public void setEdad(int edad) {
            this.edad = edad;
        }

        public int getEdad() {
            return this.edad;
        }

    ;

    }
     private static class getDatos {

        private double numMax;
        private double numMin;
        private String empleadoMax;
        private String empleadoMin;

        getDatos() {
            numMax = 0;
            numMin = 999999999;
            /* efe */
        }

        public void setNumMax(double numMax, String empleado) {
            if (numMax > this.numMax) {
                this.numMax = numMax;
                this.empleadoMax = empleado;
            }

        }

        public double getNumMax() {
            return this.numMax;
        }

        public void setNumMin(double numMin, String empleado) {

            if (numMin < this.numMin) {
                this.numMin = numMin;
                this.empleadoMin = empleado;
            }

        }

        public double getNumMin() {
            return this.numMin;
        }

        public String getEmpleadoMax() {
            return this.empleadoMax;
        }

        public String getEmpleadoMin() {
            return this.empleadoMin;
        }

    }

}
