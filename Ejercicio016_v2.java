/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.ejercicios.luisteos;

import java.util.ArrayList;

/**
 * (Usando Clases)
 * @author Luis Alonso Teos Palacios 15. Escriba un programa que produzca la
 * tabla siguiente: Escriba un programa de nómina para una compañía que tiene
 * seis empleados, algunos de los cuales son hombres y otros son mujeres. Para
 * cada empleado, el programa debe leer el nombre, el sexo, las horas de trabajo
 * y el sueldo por hora. En la primera parte de la salida de este programa se
 * debe mostrar el nombre del empleado, el número de horas trabajadas, el pago
 * por hora y el pago total. En la segunda parte de la salida de este programa
 * se debe mostrar el total y el promedio de pagos por sexo.
 */
public class Ejercicio016_v2 {

    static ArrayList<empleado> listaEmpleados = new ArrayList<empleado>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        nuevoEmpleado("Luis", "M", 150, 3);
        nuevoEmpleado("Jose", "M", 250, 5);
        nuevoEmpleado("Lorena", "F", 210, 4);
        nuevoEmpleado("William", "M", 300, 2);
        nuevoEmpleado("Rene", "M", 130, 5);
        nuevoEmpleado("Yenni", "F", 400, 4);

        System.out.println("Lista de Empleados");
        System.out.println("-----------------------------");
        getListaEmpleados();

        System.out.println("Total y Promedio de Pagos Por Sexo");
        System.out.println("-----------------------------");
        getEstadisticas();

    }

    /*funcion que solo sirve para llenar el arraylist*/
    private static void nuevoEmpleado(String nombre, String sexo, double sueldo, double horasTrabajo) {

        empleado thisEmpleado = new empleado();
        thisEmpleado.setNombre(nombre);
        thisEmpleado.setSexo(sexo);
        thisEmpleado.setSueldo(sueldo);
        thisEmpleado.setHorasTrabajo(horasTrabajo);
        listaEmpleados.add(thisEmpleado);

    }

    private static void getListaEmpleados() {
        for (empleado thisEmpleado : listaEmpleados) {
            System.out.println("Empleado: " + thisEmpleado.getNombre());
            System.out.println("Horas Trabajadas: " + thisEmpleado.getHorasTrabajo());
            System.out.println("Sueldo por hora: " + thisEmpleado.getSueldo());
            System.out.println("-------------------");

        }

    }

    private static void getEstadisticas() {
        double totalM, totalF;
        int cntM, cntF;
        totalM = 0;
        totalF = 0;
        cntM = 0;
        cntF = 0;
        /*obtenemos los datos a calcular*/
        for (empleado thisEmpleado : listaEmpleados) {

            if (thisEmpleado.getSexo().equalsIgnoreCase("M")) {
                totalM = totalM + thisEmpleado.getSueldo();
                cntM = cntM + 1;
            } else {
                totalF = totalF + thisEmpleado.getSueldo();
                cntF = cntF + 1;
            }

        }
        System.out.println("Total de Pagos en el Sexo Masculino(" + cntM + "): $" + totalM + " con un promedio de: $" + (totalM / cntM));
        System.out.println("Total de Pagos en el Sexo Femenino(" + cntF + "): $" + totalF + " con un promedio de: $" + (totalF / cntF));

    }

    private static class empleado {

        private String nombre;
        private String sexo;
        private double sueldo;
        private double horasTrabajo;

        /*geters y seters*/
        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getNombre() {
            return this.nombre;
        }

        public void setSexo(String sexo) {
            this.sexo = sexo;
        }

        public String getSexo() {
            return this.sexo;
        }

        public void setSueldo(double sueldo) {
            this.sueldo = sueldo;
        }

        public double getSueldo() {
            return this.sueldo;
        }

        public void setHorasTrabajo(double horasTrabajo) {
            this.horasTrabajo = horasTrabajo;
        }

        public double getHorasTrabajo() {
            return this.horasTrabajo;
        }

    ;

}

}
