/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author Luis Alonso Teos Palacios 6. Utilizar de referencia el problema
 * anterior, pero ahora debe tener en cuenta que el cliente tiene derecho al
 * descuento del 15% si su compra es mayor o igual a 150, en cambio, si la
 * compra fuese menor a esa cantidad sólo tendrá derecho al 5% de descuento.
 */
public class Ejercicio006 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double precio;
        double nuevoPrecio;
        Scanner in = new Scanner(System.in);

        System.out.println("Ingrese Precio del producto");
        precio = in.nextFloat();

        if (precio < 150) {
            nuevoPrecio = precio - (precio * 0.05);
        } else {
            nuevoPrecio = precio - (precio * 0.15);
        }

        System.out.println("Precio con Descuento: $" + nuevoPrecio);

    }

}
