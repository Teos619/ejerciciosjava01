/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author Luis Alonso Teos Palacios 8. Un vendedor recibe su salario con base a
 * las ventas. Si las ventas son menores de 200 recibe 200 + ventas. Si las
 * ventas son igual a 200 en total obtiene 500, pero si las ventas son mayores a
 * 200 son 500 + 10% de la venta.
 */
public class Ejercicio008 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float ventas;
        double salario;

        Scanner in = new Scanner(System.in);

        System.out.println("Digite ventas: ");
        ventas = in.nextFloat();

        if (ventas < 200) {
            salario = 200 + ventas;

        } else if (ventas == 200) {
            salario = 500;
        } else {
            salario = 500 + (ventas * 0.10);
        }

        System.out.println("Su Salario es: " + salario);

    }

}
