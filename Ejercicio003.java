/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author Luis Alonso Teos Palacios 3. Escriba un programa que pida una palabra
 * y luego la escriba tres veces en una línea.  *
 */
public class Ejercicio003 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String palabra;

        Scanner in = new Scanner(System.in);

        System.out.println("Digite una palabra");
        palabra = in.nextLine();
        System.out.println(palabra + ' ' + palabra + ' ' + palabra);

    }
}
