/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author Luis Alonso Teos Palacios 5. Una zapatería ofrece un descuento del
 * 15% sobre el total de compra y un cliente desea saber cuánto deberá pagar
 * finalmente por su compra.
 *
 */
public class Ejercicio005 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double precio;

        Scanner in = new Scanner(System.in);

        System.out.println("Ingrese Precio del producto");
        precio = in.nextFloat();

        double nuevoPrecio = precio - (precio * 0.15);
        System.out.println("Precio con Descuento: $" + nuevoPrecio);

    }

}
